# 概述

Less和原生的CSS一样，支持模块化开发，使用关键字@import

## 1. 导入准则

在原生CSS中，@import必须在所有其他类型的规则之前。但是，在Less中，可以将@import规则放在任何位置  
Less会对@import导入的文件的扩展名做以下处理：

- @import "foo";        //foo.less is imported
- @import "foo.less"    //foo.less is imported
- @import "foo.php"     //foo.less is imported
- @import "foo.css"     //foo.css is imported

```less
  .foo {
    background-color: #999;
  }
  @import "this-is-vaild.less";
```

## 2. 导入选项

Less提供了一系列的扩展来让使用@import更灵活的导入第三方文件，即为@import添加导入选项，语法格式为：*@import (keyword) "filename";*，其中keyword包含以下几个值：

1. --reference       :仅仅引用文件，但不输出
2. --inline          :在输出中包含源文件，但不加工它
3. --less            :将文件作为Less文件对象，无论原本是什么文件扩展名
4. --css             :将文件作为CSS文件对象，无论原本是什么文件扩展名
5. --once            :只包含文件一次（默认行为）
6. --multiple        :包含文件多次