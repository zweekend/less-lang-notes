# 概述

Less为了兼容CSS的原生语法，使用了原生CSS中@media媒体查询中的语法，即*when [guards]/when (default())来替代if [expression]/else或switch [expression]/case*等控制语句  
Less中的控制语句主要用于混合集的模式匹配（重载）、控制选择器的应用等

## 1. 混合集的模式匹配（重载）

> (1) guards中使用比较操作符

guards中可使用的比较操作符包括：>、>=、=、=<、<，且关键字true是唯一的真值，其他任何值都是假值

```less
  @media: mobile;

  .mixin (@a) when (@media = mobile) { ... }
  .mixin (@a) when (@media = desktop) { ... }
  .mixin (@a) when (default())

  .max (@a; @b) when (@a > @b) { width: @a }
  .max (@a; @b) when (@a < @b) { width: @b }
```

```less
  .truth (@a) when (@a) { ... }
  .truth (@a) when (@a = true) { ... }

  .class {
    .truth(40); // 应为40是假值，所以不匹配任何的混合集
  }
```

> (2) guards中使用逻辑操作符

guards中可以使用的逻辑操作符为：not（逻辑非）、and（逻辑与）和 ,（逻辑或）

```less
  // 逻辑非
  .mixin (@a) when not (@a > 0) { ... }

  // 逻辑与
  .mixin (@b) when (isnumber(@b)) and (@b > 0) { ... }

  // 逻辑或
  .mixin (@a) when (@a > 10), (@a < -10) { ... }
```

> (3) guards中使用类型检查函数

使用is函数来检查值的类型

```less
  .mixin (@a; @b: 0) when (isnumber(@b)) { ... }
  .mixin (@a; @b: black) when (iscolor(@b)) { ... }
```

## 2. 控制选择器的应用

```less
  @my-option: true;

  // 方式一
  button when (@my-option = true) {
    color: white;
  }

  // 方式二
  & when (@my-option = true) {
    button {
      color: white;
    }
  }
```