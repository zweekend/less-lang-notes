# 概述

Less中提供了一些类似传统编程语言中的操作符

## 1. 算术操作符

在Less中，任何数值、颜色和变量都可以进行运算，如下所示：

```less
  // 定义变量
  @base: 5%;
  @filter: @base * 2;
  @other: @base + @filter;
  @base-color: #888 / 2;

  // 使用
  .my-selector {
    height: 100% / 2 + @filter;
    color: @base-color + #111;
  }
```

## 2. 合并操作符

> (1) 逗号合并操作符

```less
  // Less
  .my-class {
    box-shadow+: inset 0 0 10px #555;
    box-shadow+: 0 0 5px #888;
  }

  // Output CSS
  .my-class {
    box-shadow: inset 0 0 10px #555, 0 0 5px #888;
  }
```

> (2) 空格合并操作符

```less
  // Less
  .my-class {
    transform+_: rotate(30deg);
    transform+_: scale(2);
  }

  // Output CSS
  .my-class {
    transform: rotate(30deg) scale(2);
  }
```

## 3. 嵌套操作符

> (1) &操作符

&运算符表示一个嵌套规则中，当前作用域的选择器，其是Less实现嵌套规则的核心所在，在应用修改类或应用伪类给现有选择器时较为常用，其应用场景包括：

```less
  /**
   * 将CSS层次选择器DOM化以及给现有选择器应用伪类
   */
  
  // CSS写法
  .a {
    color: red;
  }
  .a .son {
    color: blue;
  }
  .a::hover {
    color: green;
  }

  // Less写法
  .a {
    color: red;
    .son { // .son 是 & .son的简写
      color: blue;
    }
    &::hover {
      color: green;
    }
  }
```

```less
  /**
   * 生成重复的类名
   */
  
  // CSS写法
  .button-ok {
    background-color: red;
  }
  .button-cancel {
    background-color: blue;
  }
  .button-custom {
    background-color: white;
  }

  // Less写法
  .button {
    &-ok {
      background-color: red;
    }
    &-cancel {
      background-color: blue;
    }
    &-custom {
      background-color: white;
    }
  }
```

```less
  /**
   * 改变选择器的顺序
   */

  // CSS写法
  .header .menu {
    border-radius: 5px;
  }
  .no-borderradius .header .menu {
    background-color: red;
  }

  // Less写法
  .header {
    .menu {
      border-radius: 5px;
      .no-borderradius & {
        background-color: red;
      }
    }
  }
```

```less
  /**
   * 生成一个群组选择器的所有可能的选择器排列
   */

  // CSS写法
  p,
  a,
  ul,
  li {
    border-top: 2px dotted #336;
  }
  p + P,
  p + a,
  p + ul,
  p + li,
  a + p,
  a + a,
  a + ul,
  a + li,
  ul + p,
  ul + a,
  ul + ul,
  ul + li,
  li + p,
  li + a,
  li + ul,
  li + li {
    border-top: 0;
  }

  // Less写法
  p, a, ul, li {
    border-top: 2px dotted #336;
    & + & {
      border-top: 0;
    }
  }
```

> (2) 媒体查询嵌套操作符

媒体查询同样可以被嵌套在选择器中，选择器将被复制到媒体查询体内

```less
  // CSS写法
  @media screen {
    .screen-color {
      color: green;
    }
  }

  @media screen and (min-width: 768px) {
    .screen-color {
      color: red;
    }
  }

  @media tv {
    .screen-color {
      color: black;
    }
  }

  // Less写法
  .screen-color {
    @media screen {
      color: green;
      @media (min-width: 768px) {
        color: red;
      }
    }
    @media tv {
      color: black;
    }
  }
```