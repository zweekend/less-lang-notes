# 概述

类似于变量带来了管理重复出现的值的便利，*混合集*可以用来管理重复出现的*一组CSS样式*，可以通过*类选择器或者id选择器*来专门定义一个混合集来复用并进行维护，此时也称为*不输出混合集*；也可以将*某个DOM对应的类或id选择器*定义的样式作为一个混合集，此时也称为*输出混合集*

## 1. 混合集的类型

> (1) 不输出混合集

不输出混合集，最终编译后不会出现在CSS样式表中，这类混合集没有对应的DOM，专注于代码的复用以及维护

```less
  // 定义不输出混合集
  .my-mixin() {
    background-color: red;
  }

  // 用法
  .mixin-class {
    .my-mixin;
  }
```

> (2) 输出混合集

输出混合集，最终编译后同样会出现在CSS样式表中，这类混合集通常定义了某些DOM的样式，所以必须出现在最终的CSS样式表中

```less
  // 定义输出混合集
  .a, #b {
    color: red;
  }

  // 用法
  .mixin-class {
    .a;
  }
  .mixin-id {
    #b;
  }
```

## 2. 混合集的扩展

无论是不输出的混合集，还是输出的混合集，都可以通过扩展，从而使其功能得到增强

> (1) 带选择器的混合集

混合集不仅可以包含各种属性，还可以包含各种选择器

```less
  // 定义带选择器的混合集
  .my-hover-mixin {
    &:hover {
      border: 1px solid red;
    }
  }

  // 用法
  button {
    .my-hover-mixin;
  }
```

> (2) 带命名空间的混合集

可以将混合集放在一个“id”选择器里面，从而确保这个混合集和其他的库不会起到冲突

```less
  // 定义带命名空间的混合集
  #outer {
    .inner1 {
      color: red;
    }
    .inner2 {
      background-color: blue;
    }
  }

  // 用法
  .c {
    #outer > .inner1;
    #outer.inner2; // 对于">"是可选的
  }
```

> (3) 带!important关键字的混合集

在调用的混合集后面追加!important关键字，可以使混合集里面的所有的属性都继承!important

```less
  // 定义混合集
  .foo {
    background-color: @bg;
    color: @color;
  }

  // 用法
  .important {
    .foo !important;
  }
```

> (4) 带参数的混合集

```less
  /**
   * 单参数的混合集
   * 混合集也可以接收参数，在调用混合集的时候可以给混合集传递参数
   */
  
  // 定义带参数的混合集
  .border-radius (@rdius) {
    -webkit-border-radius: @radius;
    -moz-border-radius: @radius;
    border-radius: @radius;
  }

  // 用法
  #header {
    .border-radius(3px);
  }

  /**
   * 多参数的混合集
   * 混合集也可以带多个参数，并且参数之间最好以分号;分隔
   */

  // 定义多参数的混合集
  .mixin (@color; @padding, @margin) {
    color-3: @color;
    padding-3: @padding;
    margin: @margin;
  }

  // 用法
  .some .selector div {
    .mixin(#000, 3px, 2px);
  }
```

```less
  /**
   * 参数带默认值的混合集
   * 也可以给混合集的参数设定默认值，调用时，若不传递参数，则直接使用默认值
   */

  // 定义参数带默认值的混合集
  .border-radius (@radius: 5px; @margin: 5px) {
    -webkit-border-radius: @radius;
    -moz-border-radius: @radius;
    border-radius: @radius;
    margin: @margin;
  }

  // 用法
  #header {
    .border-radius;
  }
```

```less
  /**
   * @arguments变量
   * 带参数的混合集内部有一个@arguments变量，包含所有传入的参数，因此可以用来批量的处理参数
   */

  // 定义多参数的混合集
  .box-shadow (@x: 0; @y: 0; @blur: 1px; @color: #000) {
    -webkit-box-shadow: @arguments;
    -moz-box-shadow: @arguments;
    box-shadow: @arguments;
  }

  // 用法
  .big-block {
    .box-shadow(2px; 5px);
  }
```

```less
  /**
   * @rest...参数
   * 可以使用@rest...来像@arguments变量一样来接收多个参数
   */

  // 定义多参数的混合集
  .mixin (@a; @rest...) {
    // @rest is bound to arguments after @a
    // @arguments is bound to all arguments
  }
```

```less
  /**
   * 按名称传递参数
   * 引用带有参数的混合集时，可以通过参数名称而不是参数的位置来传递参数,尤其是当混合集有多个参数时
   */

  // 定义多参数的混合集
  .mixin (@color: black; @margin: 10px; @padding: 20px) {
    color: @color;
    margin: @margin;
    padding: @padding;
  }

  // 用法
  .class {
    .mixin (@margin: 15px; @color: red);
  }
```

```less
  /**
   * 模式匹配（重载）
   * 若想基于传递的参数来改变混合集的行为，则可以使用模式匹配，即将参数的值或数量写死，或者通过guards表达式序列
   */

  // (1) 基于参数值的模式匹配
  .mixin (dark; @color) {
    color: darken(@color, 10%);
  }
  .mixin (light; @color) {
    color: lighten(@color, 10%);
  }
  .mixin (@_; @color) {
    display: block;
  }

  // 用法
  @switch: light;

  .class {
    .mixin (@switch; #888);
  }


  // (2) 基于参数数量的模式匹配
  .mixin (@a) {
    color: @a;
  }
  .mixin (@a; @b) {
    color: fade(@a; @b);
  }

  // 用法
  .one {
    .mixin(#888);
  }
  .two {
    .mixin(#666; #888;)
  }

  // (3) 基于控制语句的模式匹配（详见control）
  // 若在混合集中调用自己，可循环产出多个样式规则集合
  .generate-columns(@n; @i: 1) when (@i =< @n) {
    .columns@{i} {
      width: (@i * 10% / @n);
    }
    .generate-columns(@n; (@i + 1));
  }

  // 用法
  .generate-columns(10; 1);
```

```less
  /**
   * 作为函数使用的混合集
   * 所有定义在一个混合集中的变量都是可见，可以用于调用它的作用域中（除非调用它的作用域中定义了同名变量），因此，定义在混合集中的变量可以充当它的
   * 返回值，这样就允许创建一个用起来类似函数的混合集
   */

  // 定义一个充当函数的混合集
  .average (@x; @y) {
    @average: ((@x + @y) / 2);
  }

  // 用法
  div {
    .average (16px; 15px);
    padding: @average;
  }
```

```less
  /**
   * 传递规则集合得混合集
   */
```

## 3. 混合集的作用域