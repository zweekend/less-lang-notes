# 概述

Less中允许定义变量，用来存储选择器名、属性值等有可能被大量复用的数据值，从而实现复用性，并增强代码的可维护性  

## 1. 定义

Less中使用@关键字来定义变量，如下所示：

```less
  // 定义选择器名
  @mySelector: banner;
  
  // 定义属性值
  @primary-color: #684567;
```

## 2. 使用场景

> (1) 在变量名中使用（变量插值）

```less
  // 定义变量
  @var: "fnord";
  @fnord: "I am fnord";

  // 用法
  .my-class {
    content: @@var;
  }
```

> (2) 在选择器中使用（变量插值）

```less
  // 定义变量
  @mySelector: banner;

  // 用法
  .@{mySelector} {
    line-height: 40rpx;
    font-weight: bold;
  }
```

> (3) 在CSS属性名中使用（变量插值）

```less
  // 定义变量
  @property-name: color;

  // 用法
  .widget {
    @{property-name}: #fff;
    background-@{property-name}: #999;
  }
```

> (4) 在CSS属性值中使用

```less
  // 定义变量
  @link-color: #32456b;
  @link-color-hover: #28573c;

  // 用法
  .link {
    color: @link-color;
  }
  .link::hover {
    color: @link-color-hover;
  }
```

> (5) 在URLs中使用（变量插值）

```less
  // 定义变量
  @image-url: "../img";

  // 用法
  body {
    color: #333;
    background-image: url("@{image}/white.png");
  }
```

> (6) 定义规则集

```less
  // 定义变量
  @group: {
    background-color: #fff;
  }

  // 用法
  div {
    @group(); // 调用定义规则集的变量时，必须使用()
  }
```

> (7) 在imports语句中使用（变量插值）

```less
  // 定义变量
  @theme: "../../src/themes";

  // 用法
  @import "@{themes}/tidal-wave.less"
```

## 3. 变量提升与变量作用域

- Less中，变量时延迟加载的，即变量提升，在使用前不一定要预先声明变量
- 在一个作用域内，定义一个变量多次时，只会使用最后定义的变量
- 使用一个变量时，会从当前的作用域向上搜索